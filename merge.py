#!/usr/bin/env python3
todo = open("todo.txt","r");
done = open("done.txt","r");

todo_list = todo.read().split("\n");
done_list = done.read().split("\n");

# this removes the last empty element
todo_list.pop(0);
todo_list.reverse();
todo_list.reverse();

done_list.reverse();
done_list.pop(0);
done_list.reverse();

done_list_good = [None] * done_list.__len__(); # creates an empty list with the same lenght as done_list
index = 0;
for i in done_list:
    done_list_good[index] = i[i.find("@"):];
    index += 1;

print(done_list_good);

for i in todo_list:
    print("Checking for: "+i);
    if i in done_list:
        print(i);
