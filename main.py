#!/usr/bin/env python3
import sys
print("Initializing...\nLoading config file...",file=sys.stderr);
import config # imports everything from config.py
print("Config file loaded\nLoading ics library...",file=sys.stderr);
from ics import Calendar
print("Library ics loaded\nLoading requests library...",file=sys.stderr);
import requests
print("Library requests loaded",file=sys.stderr);

url = config.url; # alias for config.url variable
print("Url set\nDownloading agenda...",file=sys.stderr);
calendar = Calendar(requests.get(url).text); # gets calendar
print("Agenda downloaded",file=sys.stderr);

events = list(calendar.timeline);

for i in events:
    task_type = i.name.split(":")[0].lower();                               # cut the string from the first character to the first `:` and make it lowercase                            # Get the task type
    subject_short = i.name.split(":")[1].split(",")[0].split(" ")[1];       # 1. Same as above 2. cut the string from the first character to the first `,` and remove the first space   # Get the short subject name
    subject = config.alias[subject_short];                                  # replaces the short form of the subject with the long form                                                 # Get the long subject name
    summary = i.description.split("\n")[0];                                 # get the first line of the description                                                                     # Get the first line of the description
    task = "@" + subject + " +" + task_type + " " + summary + " due:" + i.begin.date().__str__();                                                                                       # Joins everything together

    print(task);

